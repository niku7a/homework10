fun main() {

    val s = sayHello("Jemali")
    println(s)
    sayHello("Sveta")
    sayHello("Gulnazi")

}

fun sayHello(name: String): String {
    return "Hello $name!"
}

